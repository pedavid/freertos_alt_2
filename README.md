# FreeRTOS_Alt_2

Trabajo practico de FreeRTOS para técnicas digitales 2, curso R4051

Primeros 20 numeros. Se ejecutó el codigo en la bluepill. Devuelve siempre estos 20 numeros.
Si uno conoce las semillas es posible saber que numero saldrá, pero es especialmente sencillo en estos primeros
20 valores saber cual será el proximo led a prenderse.

0xf8e0be64
0x185b28
0xf8e0a17b
0x3a1b7bac
0xc7f5f115
0x5f633d8
0xf8e0a164
0x19c763cf
0x71d244bb
0xc50ed1aa
0x3818a3ee
0x1ae36639
0xf9f70e42
0x4ac71357
0xb73dce32
0xb6eea442
0xf72a29c3
0x8505c1da
0xdc10c316
0x1f1fa062
0xb9ddf673
